# IUVIA Docs

Documentation of the IUVIA project.

[See the website](https://thedrastical.gitlab.io/iuvia/docs/)

This documentation is created with [Sphinx](https://www.sphinx-doc.org/en/master/)
Using the theme [Sphinx Book Theme](https://sphinx-book-theme.readthedocs.io/en/latest/layout.html)


## Read this documentation locally 

Once you've cloned this repo, create a python virtual environment and install the dependencies:

```
$ pip install virtualenv
$ virtualenv venv
$ source venv/bin/activate
$ pip install sphinx
```

To build the `html` documentation files, go to the `docs` folder run make

```
# To make the html documentation files including the embedded To-Do notes and To-Do list file
$ make html

# To make the html documentation without the To-Do notes
$ make O="-D todo_include_todos=0" html
```

The html files will be created under the directory `_build/html`

## Contribute to this documentation

To help contribute please read the [contribution guidelines](https://thedrastical.gitlab.io/iuvia/docs/contributor/guidelines/index.html) in the documentation.
