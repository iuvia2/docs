.. _admin_management_guide:

******************
Working with IUVIA
******************

So you have your IUVIA Platform setup: now you are ready to manage your
self-hosting solution, explore and install apps, manage the device settings (if
you are hosting it on the IUVIA Device) and make it available to other trusted
users.


.. figure:: /_static/images/iuviadevice.png
   :width: 300px
   :align: center
   :alt: IUVIA Device

   You got yourself a IUVIA!


Users and permission
====================

A IUVIA platform is linked to a IUVIA account, but may have multiple user
profiles that you, as an administrator, can create and manage, in order to allow
any trusted person the use of all the IUVIA functionalities.

When creating a user account, you will be asked to create an authentication
passphrase for the user, provide a username and an email and other aditional but
optional information.

If the IUVIA email application is installed and the domain name configured, any
user existing in the device will have their own ``<username>@<domain.com>`` email.

As an administrator you can also limit the amount of disk space that any user is
allocated, the apps that the users can see and use, and give admin privileges to
any user that you choose.

Only users with admin privileges will be able to install, upgrade or delete
applications from the device.


Domain management
=================

While signing up for your IUVIA subscription, you will be asked to specify a
domain that you own and that you want to be linked to your device. This domain
will be used to make your IUVIA environment accessible from the Internet, and to
configure your email accounts.

If you don't have a previously purchased domain, you can let us handle the
registration, which will be part of your subscription process.

The domain linked to your self-hosted IUVIA platform can be changed at any time
for any other domain that you own.


Device management
=================

If you got yourself a IUVIA Device, congrats! Our device is not another boring
NAS to host your files, but a piece of hardware with a lot of potential. Thanks
to the onboarding setup experience, it automates the setup process and in a few
steps you will have a self-hosted device connected to your home network to which
you can access from anywhere. You can read more about this in the :ref:`setup guide
<admin_setup_guide>`.

Once your device is set up, you can access to its configuration through the
BoxUI interface (you'll read more about this later) and alter the device
settings, configure its power management behavior, the external appearance
(color and behavior of the LED notifications), access the data about your power
consumption, and configure emergency related behavior (panic button!).


App lifecycle
=============

The IUVIA Platform is configured to include a set of default applications in the
system. These are applications that provide the most commonly used services that
we, at the IUVIA Collective, consider that a default configuration of IUVIA must
have. These apps are:

* P2P file storage and backup application
* Email server and web-based clients
* Calendar and contacts applications
* Nextcloud file storage and collaborative edition

Any application can be installed, upgraded or removed from the IUVIA Platform by
any user with admin privileges. BoxUI (the IUVIA interface) allows you, as an
admin, to list and manage the installed applications, and to access the
marketplace to explore existing 3rd party features and install them in your
platform.


Marketplace
===========

The IUVIA marketplace is an innovative approach to an App Store by using
decentralized technologies to make it privacy-by-design and offline-first. Not
only we provide a space for FOSS projects to distribute their projects, like
F-Droid does, but by using P2P protocols we are implementing a privacy-friendly
app repository in which a single authority or server cannot have complete
awareness of the applications that the users download or install. Similarly, a
user can choose to install and update applications in an environment that
doesn't need to be connected to the Internet, only a locally-discoverable peer.

As an administrator, BoxUI allows you to access and navigate this marketplace,
check out new apps added to the ecosystem, navigate through categories, explore
their information and download them into your device, either to seed them from
your local repository or to install them into your device.

As a member of the decentralized free software ecosystem, the IUVIA marketplace
lays down an infrastructure for you to publish your decentralized applications
and make them available for any users in the IUVIA space to download, seed,
install and enjoy your apps. If this is the case, you can read everything about
it in the :ref:`contributor guide <contributor_guide>`

Currently the P2P Marketplace uses the `Dat protocol
<https://docs.dat.foundation/docs/intro>`_ and is packaged as an `AppImage
<https://appimage.org/>`_. You can read a bit more about its implementation in
the `Marketplace GitLab repository <https://gitlab.com/thedrastical/iuvia/dat-appstore/>`_ and the `Marketplace API documentation <https://thedrastical.gitlab.io/iuvia/dat-appstore/>`_


BoxUI: The Interface to IUVIA
=============================

BoxUI is the Platform Management Interface that allows you to create and edit
users, handle hardware-related settings, manage your network connectivy and add,
remove and update functional services through the app marketplace.

You can login into BoxUI using the credentials created during the onboarding
process or the admin user created during the manual installation of the IUVIA
Platform.

Once you are logged in into BoxUI, take a look around. From the main menu, you
have direct access to the following screens:


.. figure:: /_static/images/m01.png
   :width: 300px
   :align: center
   :alt: BoxUI login screen

   BoxUI login screen


Dashboard
---------

The dashboard is the homepage of BoxUI and here you can find most of the things
that you will need on a regular basis:

* The most frequent and latest actions.
* Charts to let you visualize your power consumption, disk and RAM usage, and
  other useful information.
* Unread notifications: errors, warnings, availability of new apps or system
  updates, and much more.


Actions
-------

Actions are the basic component of BoxUI and what allows users to use the
functionalities provided by installed apps. Actions generally make the user
navigate away from BoxUI but can also provide the user with information
necessary for external configuration processes.

Actions are always tied up to Apps, and an installed App can display a number of
Actions for the user to launch. Actions are also platform-specific, wich means
that an installed app can have different Actions that will be available or not
depending on the device that you use to access BoxUI.

For example, a calendar app, once installed, might show in your Action screen a
couple of different icons:

*  If you access through your mobile phone, you will see an Action to sync your
   appointment data with your phone client and another one to open in your
   browser a web application to explore and manage your calendar.

*  However, if you access through your computer, you won't see the calendar sync
   Action, only the calendar browser-based application one.

You can read more information about the difference of Actions and Apps in our
:ref:`glossary <glossary>`

Apart from the section in the Dashboard with a quick access to the latest and
most frequently used Actions, you can navigate through all the Actions
available, search, filter and see information about them on the Action screen.


.. figure:: /_static/images/boxui1.png
   :width: 600px
   :align: center
   :alt: BoxUI dashboard and actions pages 

   BoxUI dashboard and actions pages


App Marketplace
---------------

The third main screen of the BoxUI interface access the P2P Marketplace, where
you can navigate and discover new 3rd party applications that the decentralized
deeloper community releases into the IUVIA space. This interface allows you to
search in the repositories, explore different app categories and download into
your device for inmediate installation or simply to seed the application and
maybe install it in the future in any offline circumstance.


Settings
--------

The configuration screen in the BoxUI interface contains the access to all the
necessary configuration of your IUVIA Platform and Device:

* Device Settings
     * DNS Services
     * On/Off Behavior
     * Backups
     * Energy Consumption
     * Diagnostics
     * System Updates

* Subscription Management
     * Account
     * Domain
     * Payments
     * Billing

* User Management
     * Profile Management
     * User Permissions
     * Language
     * Date and Time

* App Management


.. figure:: /_static/images/boxui2.png
   :width: 600px
   :align: center
   :alt: BoxUI marketplace and user management pages

   BoxUI marketplace and user management pages


