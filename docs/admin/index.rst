.. _admin_guide:

#########################
IUVIA Administrator Guide
#########################

This is the documentation for anyone who is an Administrator of a IUVIA Device
or is running the IUVIA Platform in their own hardware. As an administrator you
will be able to handle your device settings, configure its behavior, share
it with your trusted peers by creating and managing user accounts, and finally
but most importantly, control how IUVIA will be used by adding any amazing
functionalities that you want through the App Marketplace.

In this section of the documentation you will learn how to set up your device
and what all things you can do to leverage its full potential.

Let's get self hosted!


.. toctree::
   :maxdepth: 2
   :caption: Contents

   setup/index
   management/index
