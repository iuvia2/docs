.. _admin_setup_guide:

**********************
Installation and setup
**********************

If you have decided to join the self-hosting cause by trying out the IUVIA
Device... Thank you! Here we guide you through the process of creating your
account, purchasing the device and setting it up in your premises and under your
new (or existing) domain.

If you want to use the IUVIA Platform over your own hardware, you will also be
able to do this following :ref:`this
guide <contributor_guidelines_platform_setup>`.


IUVIA Account
-------------

You will need a IUVIA Account to purchase the device, set up and process your
payment, and configure any external services that might be needed (domain,
VPN). A IUVIA Account is identified by a unique subscription ID that you will be
provided once completed your purchase process.

The steps for acquiring a IUVIA Device are the following:

1. Enter our `online shop <https://shop.iuvia.io>`_ (Coming soon): Here you can see all the
   IUVIA Device purchase options, explore different configurations and their
   prices.

2. Choose your device

3. Enter your address information: We need this information in order to send you
   your new device. Once your order has been placed, the address will not be
   kept as part of your account information.

4. Enter your invoice information: This is required to produce a legal invoice.

5. Select your payment method: We offer several payment methods, select the
   preferred one and enter the necessary information for us to process the
   payment.

6. Check your subscription information and finalize the payment

7. Note down your subscription number: Each subscription has a unique
   subscription identifier associated to it. We will provide you with this
   number, which you will need to activate your subscription once you receive
   and set up the device.

8. Purchase or setup your domain: In order to set up your email and other
   services on the device, we need you to have your own domain. If you don't
   have one yet, you can buy it through us at this point.


What data do we need from you?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We need your **payment information** for both the device and regular
subscription payments, and your **full address** to send you the IUVIA Device.


How can you purchase IUVIA anonymously?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We want to allow you protect your privacy, that is why we brought IUVIA to life.
If you want to receive and use the IUVIA Device, we need some data from you, but
you have the chance of preserving your anonymity through the whole process. If
that's your intention, we can suggest you few ways of doing that.

We need your address so that you can receive your newly purchase device, but we
don't really want to know where you live. You can choose to give a non
identifiable address where you can go and pick up your device, such as a post
office or a private package reception center.

You will be offered several payment methods out of which you can choose the most
convenient to you. If you don't want to give personal data, we also accept
prepaid cards. To read more about this payment option, you can visit `this link
<https://en.wikipedia.org/wiki/Stored-value_card>`_



Onboarding and initial setup
----------------------------

You have now received your IUVIA Device at home (or at any other place): Wow,
that's exciting, congratulations! Let's set it up now.

.. figure:: /_static/images/onboarding1.png
   :width: 500px
   :align: center
   :alt: Welcome to IUVIA

   Welcome to IUVIA

.. todo::
   Add more precise information about how the onboarding experience starts when
   you plug IUVIA

Once you plug your IUVIA Device, you'll be able to connect to it following the
guide that you will receive along with it.

Once you see the IUVIA welcome screen, you will be guided to go through the
onboard setup wizard, which will take you through different steps and provide
you with information about why these steps are needed.

1. Set your encryption key

2. Set up the network

3. Enter your subscription number

4. Read carefully the guide about **privacy related decisions**!

5. Configure your preferred level of **privacy vs convenience**

6. Select a username and password to log into your device

7. You are good to go! You are ready to read through the
   :ref:`admin_management_guide` chapter

.. figure:: /_static/images/onboarding2.png
   :width: 500px
   :align: center
   :alt: Setup finished successfully

   Setup finished successfully


What does it mean privacy vs. convenience?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Privacy is not an easy task, hence we are working to make it accessible to you,
no matter what your level of technical knowledge you have. However there's
always a privacy vs. convenience balance to consider. Our approach is to make it
very clear to you and allow you to decide: Do you prefer a more convenient
configuration, or do you place privacy as your highest priority?

**High convenience with some privacy tradeoffs:**

* Direct connection to device if possible
* Relay through our network if not possible
* P2P connections to marketplace
* Backups settings with us

**Less convenience and no privacy tradeoffs:**

* Connection to the device only through Tor
* Marketplace connection through Tor proxy
* Setting backup on USB drive

.. figure:: /_static/images/onboarding3.png
   :width: 500px
   :align: center
   :alt: Privacy vs. convenience

   Privacy vs. convenience

.. todo::
   Should we expand on these? Maybe explain it a little bit more instead of
   putting the list
