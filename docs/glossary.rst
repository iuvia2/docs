.. _glossary:

Glossary
********

To try to convey clear meanings of some concepts in this project, we
present the following glossary. All terms linked to the glossary shall
be understood by their definition here. Contributions are welcome and
encouraged to improve the definitions where appropriate.

.. glossary::

   IUVIA Product
      The deliverable that gives the user the core value proposition
      of this business branch. The THING THAT HAS A NAME and
      marketing. E.g. IUVIA Device, packaging, instructions, welcome
      book, tutorials, advertising, warranty, etc.

   Hardware platform
      Self-explanatory

   Software platform
      Bootloader plus OS Base Image plus all System-Apps

   Device
      :term:`Hardware platform` plus :term:`Software platform` of the
      :term:`IUVIA Product`

   Base Operating System
      A set of kernel + base distro-OS packages that allow the
      existence of the :ref:`IUVIA Platform <contributor_software_platform>`
      that our :term:`IPA`\s assume will exist. The base operating system is
      expected to be immutable except for upgrades.

   IPA
   IUVIA Platform Application
      An application that has some properties.

   AppImage
      An application that is packaged in AppImage format. The marketplace
      has THESE things, maybe not exclusively (some of these things
      may not be available in the marketplace, e.g., dev-/alpha-
      versions of these things).

   AppImage Repository
      A P2P-based content storage that contains versioned AppImages
      with an index of what OS Base Image version is each AppImage
      compatible with. Depending on technology, a device may store
      partial copies of the contents in the AppImage Repository
      locally.

   AppImage Local Repository
      The part of the AppImage Repository that is locally-available in
      a device for installation, check, audit, or other operations,
      including operation over the related metadata. The contents here
      can also be shared with other peers in the AppImage Repository
      in the P2P network.

   Marketplace
      A user-facing and user-friendly, navigable interface to the
      AppImage Repository that exposes available content (including
      updates) of Marketed Apps.

   Marketed App
      An AppImage that has been deployed in the AppImage
      Repository. It provides a service that directly adds user value
      over the core value proposition of the product. E.g., "Contacts
      and calendar app, Email generic app or Email server and/or
      client independent apps (depending on what we decide in the
      future)"

   System App
      AppImage available in the AppImage Repository that provides part
      of the value proposition of the product. It is not possible to
      uninstall it, but it could be upgraded via similar mechanisms as
      a Marketed App. E.g. BoxUI

   Default App
      Marketed App installed on the device by default. It is possible to uninstall it.

   Action
      BoxUI UI component that makes the user navigate away from BoxUI
      in order to perform some other AppImage-specific use cases. A
      launcher is tied to an AppImage, although the same AppImage
      might create none to N launchers. Actions are
      platform-aware. This means that two different launchers may
      perform the same action, but with different means or for
      different platforms (e.g, Nextcloud native-app on
      mobile, v. Nextcloud webapp on "browser" platform).

   Dashboard
      BoxUI view component. Provides a way to access the rest of the
      BoxUI application and maybe beyond BoxUI via Actions if we put
      them there (TBD).


   SSO
   Single Sign-On
      A technique that allows applications to share credentials. By
      authenticating through a single "login" application, such
      application can pass the authentication credentials to the
      underlying apps, preventing the user to authenticate multiple
      times to attest their identity.

   OIDC
   OpenID Connect
      OpenID Connect is a standard technology for delegating
      authentication supported by many applications, like OAuth.

   LDAP
   Lightweight Directory Access Protocol
      LDAP is an open vendor-neutral application protocol for
      accessing directory information services. Think of it as
      protocol to access something alike a telephone guide, but which
      can also authenticate the users using the same protocol. That
      is, if you are a user in the telephone guide, you can look for
      yourself and the protocol can authenticate you. It is sometimes
      used for authentication and to have a central place to manage
      users, email addresses and aliases and other entities.
