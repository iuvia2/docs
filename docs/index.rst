Welcome to the IUVIA Project documentation!
===========================================

Hello! Welcome to the IUVIA Project docs. Through this documentation
you will be able to find everything you need to know about IUVIA.

.. figure:: /_static/images/iuvita-hi.png
    :width: 200px
    :align: center
    :alt: Welcome to the IUVIA docs

If you are a user and want to explore the possibilities of a self-hosting
solution or want to learn how to use IUVIA go to the :ref:`IUVIA User Guide
<user_guide>` (Coming soon).

If you are a system administrator and want to know how to set up and manage your
IUVIA Platform, over the IUVIA Device or over your own hardware go to the
:ref:`IUVIA Admin Guide <admin_guide>`.

Finally, if you want to contribute to the IUVIA Project, either contributing to
the codebase or adding your application to the IUVIA Marketplace go to the
:ref:`IUVIA Contributor Guide <contributor_guide>`.

You can also visit the documentation :ref:`index <genindex>` or
:ref:`search<search>` through its pages.


Contents
========

.. toctree::
   :maxdepth: 4

   overview/01-project-overview
   admin/index
   contributor/index
   glossary
