from docutils import nodes
from docutils.parsers.rst import directives

from sphinx.util.docutils import SphinxDirective


def make_label(name, rawtext, text, lineno, inliner, options={}, content=[]):
    """Link to Docs page with icon."""
    app = inliner.document.settings.env.app
    parts = text.split(':')
    if len(parts) == 1:
        className = '-'.join(text.split(' '))
        labelText = text
    else:
        className = parts[0]
        labelText = parts[1]
    label = nodes.inline('', labelText)
    label['classes'] = ['badge', className]
    return [label], []

def setup(app):
    app.add_role('label', make_label)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
