from docutils import nodes
from docutils.parsers.rst import directives

from sphinx.util.docutils import SphinxDirective


class repo(nodes.Admonition, nodes.Element):
    pass


def visit_repo_node(self, node):
    self.visit_admonition(node)


def depart_repo_node(self, node):
    self.depart_admonition(node)


class RepoDirective(SphinxDirective):
    """A custom directive that describes a repository."""

    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    has_content = True
    option_spec = {
        'tags': directives.unchanged_required,
        'class': directives.class_option
    }

    node_class = repo

    def run(self):
        text = '\n'.join(self.content)  # Content
        title_text = '\n'.join(self.arguments)

        repo_node = self.node_class(text, **self.options)  # Create admonition node

        textnodes, messages = self.state.inline_text(title_text, self.lineno)
        title = nodes.title(title_text, '', *textnodes)

        title.source, title.line = (self.state_machine.get_source_and_line(self.lineno))
        repo_node += title

        repo_node['classes'] += ['repocard']
        if 'class' in self.options:
            repo_node['classes'] += self.options.get('class', [])

        if 'tags' in self.options:
            tags = nodes.bullet_list()
            tags['classes'] += ['taglist']
            for tag_text in self.options.get('tags', '').split():
                tag = nodes.list_item()
                tag.append(nodes.Text(tag_text))
                tag['classes'] += [tag_text]
                tags.append(tag)
            repo_node += tags

        self.state.nested_parse(self.content, self.content_offset, repo_node)

        return [repo_node]


def link_gitlab(name, rawtext, text, lineno, inliner, options={}, content=[]):
    """Link to Gitlab page with icon."""
    app = inliner.document.settings.env.app
    icon = nodes.emphasis('', '')
    icon['classes'] = ['icon-git']
    node = nodes.reference(
        '',
        '',
        refuri=text,
        **options
    )
    node['classes'] = ['icon']
    node.append(icon)
    return [node], []


def link_docpages(name, rawtext, text, lineno, inliner, options={}, content=[]):
    """Link to Docs page with icon."""
    app = inliner.document.settings.env.app
    icon = nodes.emphasis('', '')
    icon['classes'] = ['icon-book']
    node = nodes.reference(
        '',
        '',
        refuri=text,
        **options
    )
    node['classes'] = ['icon']
    node.append(icon)
    return [node], []


def setup(app):
    app.add_node(repo,
                 html=(visit_repo_node, depart_repo_node),
                 latex=(visit_repo_node, depart_repo_node),
                 text=(visit_repo_node, depart_repo_node))

    app.add_directive('repo', RepoDirective)

    app.add_role('docpages', link_docpages)
    app.add_role('gitlab', link_gitlab)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
