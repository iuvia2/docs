################
Project overview
################


What is the IUVIA Project
=========================

The IUVIA Project is a privacy-by-design solution that allows anyone to own
their data phisically by hosting their cloud-like services at home and enjoy all
the operative benefits of the cloud without its privacy and security issues.

IUVIA aims to democratize access to self-hosting, without the need of technical
knowledge.

The IUVIA Project involves the development of both a hardware device that
customers can purchase and easily install at their premises, and of the IUVIA
Platform, a Debian-based OS (ChubascOS) with a set of tools to manage the device
and set up and manage a self-hosting infrastructure.

Added to this, the IUVIA Platform also consists on the development of a
distributed repository of self-contained applications and all the necessary
protocols and specifications to package and distribute these apps.


What is the IUVIA Device?
=========================

The IUVIA Device is a hardware reference device that can be purchased
with the IUVIA Platform pre-installed (and can be built from open
documentation from off-the-shelf components).

The IUVIA Device is not a hard requirement for running the IUVIA
Platform, and you may be able to find different devices that could
work. For example, an old laptop or other machine or server you
may have lying around may be fine. However, in order to make it easy
to offer support and to ease replicability, we are providing the IUVIA
Device as reference hardware and to help adoption, diminish
the chance of incompatibilities, and make it easier to self-host
without requiring a lot of tech knowledge.

In other words: the IUVIA Device is defined, designed and manufactured
so that we can provide hardware guarantee and software support for
the system. If you're adventurous, you're much more on your own, but
it can be fun too.


The IUVIA Device is built with the following principles in mind:

* Open hardware
* Green approach:
   * Lower obsolescence
   * Reduce power consumption
   * Power monitoring
* Friendliness and accessibility:
   * Ease of use and setup
   * Sleek enclosure design

.. todo:: Shall we expand on the IUVIA Device building principles?

To know more about the IUVIA Device specifications, jump to the
:ref:`contributor_hardware_overview` chapter.


What can you use the IUVIA Platform for?
========================================

With the IUVIA Platform running over our own hardware or over the IUVIA Device,
you can have a self-hosting solution at your home (or anywhere else you choose).
Using your own Internet Domain (whatever-you-want.example) you can have email
accounts, files, phone photo backups, and real-time file collaboration and
sharing, among other things.

There is no hard limit on how many of those you can have, just the limitations
you may have on bandwidth and storage space.

As the IUVIA ecosystem grows, our distributed marketplace (app repository) will
continue to grow to provide users of a variety of apps which are aligned with
the IUVIA Project goals of data sovereingty. Through the IUVIA Platform, you
will be able to easily navigate through the repository and install, update and
use any app that provides you with cloud-like services and still keep your data
private and protected.

If you  part of the free software, data privacy, decentralized software
ecosystem, the IUVIA Platform is also a great way to be part of a cohesive
system, reach more users and spread your project vision. We have designed an
open protocol for packaging, distributing and communicating with the platform
so that projects like yours can be offered, explored and promoted through the
IUVIA Marketplace.

To know more about how to package your app, you can check the
:ref:`contributor_appimages` chapter.


Why does this project exist?
============================

.. todo::
   This is taken from OTF application, I thought this comprised really good what
   we wanna say here. Maybe we want to cut it down a bit?

This project is part of facilitating digital sovereignty to citizens around the
world. In order to do that, we are pushing towards physical data sovereignty:
owning your data physically in a place you trust, under the jurisdiction you
choose to know.

We picture IUVIA as a catalyst for change by being a point of exchange between
different free software communities, and driving innovation forward from
cooperativism through free software.

As part of that change, and given the right circumstances, we believe in
building an economically-sustainable free software community that provides
privacy and digital ownership, led through democratic structures and
cooperativism, both internally and with other communities.

Through our platform and by leveraging overlay networks such as Tor, we aim to
provide better conscience on how to use secure networks and to influence
software development to take into account these nuances, as well as being able
to refer to a platform that can help them deploy their solutions under a
framework that provides certain security guarantees.

In order to achieve that, and to define the platform itself, we aim to generate
a set of general standards and conventions to package, distribute, deploy and
lifecycle of open-source projects that give the user more technological
sovereignty and less lock-in to particular companies, with a particular emphasis
on distributed models of networking and friend-to-friend networking.

These standards should be general enough so that other projects with a more
narrow scope could benefit from using them and even contributing back with their
own needs, strenghtening the mechanisms we have for cooperation.

We envision this project as the "glue" between the different feature-centric
projects that provide solutions to specific needs in isolation, and bridge them
as a more coherent alternative, in order to compete with cloud services that can
give such experience from a centralized perspective.



