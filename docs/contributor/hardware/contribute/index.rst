How to propose changes to the designs
#####################################

1. Things that need to be officially certified:
    - Our QA/QC thing
    - Somebody needs to pay for the certification
    - External agency QA/QC and certification
2. Things that need not be officially certified:
    - Our QA/QC thing
