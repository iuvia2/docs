Hardware Platform
#################

The IUVIA Hardware Platform comprises a PC-standard system that is capable of
running Linux, and additional devices and peripherals to provide additional
features. In order to provide a useful value to our customers, hardware has been
selected for its performance and performance-consumption ratio, within the
x86_64 environment. The reasons behind choosing the x86_64 Instruction Set
Architecture processors for the IUVIA Device are its greater availability,
performance, replaceability and overall knowledge.


Hardware Selection
******************

.. todo::
   Explain the hardware pieces selected for the IUVIA Device

Components
==========

The board
---------

For the board we select an ODROID H2, an Intel J4105 2.3GHz quad core CPU with
up to 32GB of DDR4 RAM, 1x NVMe, 2x SATA, USB 3, Gigabit ethernet and GPIO
ports. 110mm x 110mm x 47mm form factor.

ioboard
-------

We extended the platform with custom hardware that we collectively call ioboard.
The ioboard is an electronics platform built over an ESP8266 chipset controlling
LEDs, sensors and testing WiFi connections without losing connection with the
main WiFi adapter. The hardware schematics also
`available <https://gitlab.com/thedrastical/iuvia/ioboard>`_ as is the
`firmware <https://gitlab.com/thedrastical/iuvia/ioboard-firmware>`_, enabling
replicability for other projects.

The firmware is GPLv3-licensed, which is aligned with both our business goals
and the goals of mutually-beneficial collaboration on the system.

Hardware constraints
====================

To select the components for the IUVIA Device hardware components we defined the
following constraints

Open-Source Friendliness
------------------------
Hardware must be open-source friendly. We wanted to use hardware from companies
that contribute to open source projects, offer good support for open source
operating sys- tems, and if possible publish their hardware designs as open
source.

Power balance
-------------
Computational power / power consumption. We wanted a good balance between power
and energy usage, and also a device that can run silent (passive cooling).

Platform
--------
Software availability is a must, and computational power is very important when
you want to run multiuser apps and cryp- tographic software. So after some
testing with popular platforms like Raspberry Pi, Rock64, and others, we decided
to use a platform based on x86_64 architecture.

Expansion and I/O
-----------------
The device must have standard I/O ports for fast pe- ripherals (SATA, PCIe, USB
3...), so the device can be upgraded with standard off-the-shelf hardware, and
standard hardware ports (UART, I2C...) for expansion with custom hardware.

Form factor
-----------
The size and port placement of the board is important for the enclosure design,
so we wanted the smallest board possible.

Availability
------------
The hardware must be available now and in the near future, so we can buy it in
bulk from the supplier, and so that we can guarantee replacements for defective
units.

Price
-----
We need to keep hardware price low so that we don’t underesti- mate our margins
