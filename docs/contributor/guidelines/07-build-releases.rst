Building a release from source
##############################

1. Download code
2. Pass tests
3. Make the build
4. Distribute release (dat/Gitlab CI/whatever)
