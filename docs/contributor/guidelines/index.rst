***********************
Contribution Guidelines
***********************

.. toctree::
   :maxdepth: 1

   01-what-to-do
   02-getting-in-touch
   03-code-of-conduct
   codebase/index
   platform-setup/index
   08-app-whishlist
