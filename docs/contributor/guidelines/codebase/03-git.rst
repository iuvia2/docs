Git guidelines
==============

Before you start to collaborate with us, read this guide and learn our
guidelines on how to submit code, how to report bugs or even suggest new
features.

Reporting issues
----------------

Have you found a bug? Or maybe something that you expect to do, but cannot, in
the IUVIA platform? That's great news! We are growing and we'd love to know
about your experience, so please, let us know. Here are few things to consider
before and while reporting an issue.

.. figure:: /_static/images/iuvita-bug.png
    :width: 350px
    :align: center
    :alt: Help us find bugs!

    Help us find bugs!


Where do I report it?
^^^^^^^^^^^^^^^^^^^^^

IUVIA Project has :ref:`many repositories <contributor_repositories>`, so to
make life easier to our users and testers, we have centralized issue tracking
into one: `<https://gitlab.com/thedrastical/iuvia/issues>`_


How do I report it?
^^^^^^^^^^^^^^^^^^^

To report an issue you must follow the following three steps:

1. Review the issue tracking repository and make sure that what you are about to
   report hasn't been reported already. If you find the same issue in the board,
   feel free to participate in the discussion by adding details regarding your
   experience.

2. If the issue hasn't been reported, write your report following the
   guidelines detailed below and send your report to our Service Desk email:
   `incoming+thedrastical-iuvia-issues-19408389-issue-@incoming.gitlab.com
   <mailto:incoming+thedrastical-iuvia-issues-19408389-issue-@incoming.gitlab.com>`_

3. Keep track of the issue you submitted and help us with information. Our team
   will probably follow up and ask you more details about how, when and where
   you encountered the issue, so it would be great help if you could reply to
   their questions and help further to pin it down.


Bug or feature request?
^^^^^^^^^^^^^^^^^^^^^^^

Good question! They are not the same thing!

It is really important for us to find and fix all existing errors. We also
understand that you want to see IUVIA grow, so we want you to ask us for
enhancements and new features too. You can ask for the same through our `Service
Desk email
<mailto:incoming+thedrastical-iuvia-issues-19408389-issue-@incoming.gitlab.com>`_,
but clarify whether your request is an enhancement or a bug report.

**A bug is** when something is broke or malfunctions. You can report a bug when
something that is expected to work or was working before is not working now.

**An enhancement is** when something that you consider useful or even necessary
is not available in the currently offered features of the IUVIA platform or
device. Sharing with us your enhancement requests is extremely important to us,
as we want to grow providing things that are important for our users, but an
enhancement request will always have less priority than a bug fix, and we expect
you to understand that.

If you still aren't clear on whether your report is that of a bug or of an
enhancement request, you can always ask us! Go to :ref:`our contact section
<contributor_get_in_touch>` to know where to find us.


How to report a Bug?
^^^^^^^^^^^^^^^^^^^^

Great! Let's report it.

Make sure that you can explain the steps to our team to reproduce the error and
start creating your report with our bug report template. You can find this
template in the folder `templates
<https://gitlab.com/thedrastical/iuvia/issues/-/tree/master/templates>`_ of our
issue tracking repository.

Once the template is filled, send an email to our `Service Desk
<mailto:incoming+thedrastical-iuvia-issues-19408389-issue-@incoming.gitlab.com>`_
with the line "**[Bug] Short title about yur bug**" as Subject of the email.

In the bug report template you'll find the following sections:

**Description**

Detailed description of the bug. Explain to us what happened, what were you
trying to do and what is the error that you faced. Attach to this all the
information that you have, you can add screenshots and any error messages that
you got (if any)

**Steps to reproduce**

This should be a clear list of steps that we will follow to reproduce your
error. Try to reproduce it again before reporting it to us to have a clear idea
of how to describe this list. Let us know where to start from and a concise and
detailed list of steps in `Markdown list format
<https://www.markdownguide.org/basic-syntax#lists>`_. The list should end with
the error that you are facing.

**Expected behavior**

A concise description of what behavior you were expecting.

**Environment**

Let us know your full setup. Are you using IUVIA Device? Are you using the IUVIA
platform over your own hardware? Did you find this error while working on a
development environment?

Add full information of the system setup (hardware, OS, IUVIA release versions,
etc.) over which this error happened.


How to send an Enhancement Request?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Fill up the enhancement request template that you'll find in our `templates
<https://gitlab.com/thedrastical/iuvia/issues/-/tree/master/templates>`_
directory in the issues repository.

Once the enhancement is well detailed, send an email to our `Service Desk
<mailto:incoming+thedrastical-iuvia-issues-19408389-issue-@incoming.gitlab.com>`_
with the line "**[Enhancement] Short title about your request**" as Subject of
the email.


Submitting code contributions
-----------------------------

We are looking forward to accepting contributions! Do you want to start
collaborating with us? You can find out what you can do by checking out our
issues board and exploring those tagged by us as ``help-wanted``. You will see
issues tagged differently depending on the difficulty and the technologies
needed. We encourage you to start with a ``beginner`` issue.


First of all, make sure that you read this guide and follow the guidelines, they
are important to preserve the following goals:

* Preserve or improve IUVIA quality.
* Keep it human-centered, Fix problems that are important to users.
* Engage the community, work for the common good.
* Keep IUVIA a sustainable project.


Comment on the issue
^^^^^^^^^^^^^^^^^^^^

Let us know that you are working on the issue so that we all avoid duplicating
efforts! You can comment the solution that you plan to implement, ask for
opinions and iterate on your ideas before starting to code.


Find the correct repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Our issue board is centralized, but our codebase is separated into many
different :ref:`git repositories <contributor_repositories>`. The issue will probably
already contain information of what repository is involved in this (we will make
sure of that), but if it's a new issue, we might have not added this information
yet. If that's the case, make sure that you know exactly where is the code
involved. You can ask in the issue or :ref:`contact us <contributor_get_in_touch>`
to have a chat about it.


Make yourself comfortable working with Git and GitLab
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We work with Git and all our code repositories are hosted in `GitLab
<https://gitlab.com/>`_. To contribute to our codebase, you should be familiar
with Git and have a GitLab account. If you reached this place, you will probably
already know how to work with Git, but if that's not the case, you can read
`this really easy and summarized guide to git
<https://rogerdudler.github.io/git-guide/>`_ or something more extense like the
`git guide for contributing to a project
<https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project>`_.

Once you are comfortable working with Git, make sure that you have a GitLab
account. GitLab is an open-source Git forge, you can register for free and start
contributing to our repositories.


Set up the development environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Before starting to work, make sure that your development environment is up and
running. This means that you'll have to get comfortable with more repositories
than the one that you are about to work on. :ref:`Follow this guide
<contributor_dev_environment>` to set up the development environment before
starting to work.


Steps to contribute to our code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once you have all set up, you are ready to go! These are the steps that you'll
need to follow.

1. **Make a fork of the repository** that you want to work on. While setting the
   environment you will have a local clone of few of the repositories working
   together, for example `ChubascOS
   <https://gitlab.com/thedrastical/iuvia/chubascos>`_ and `BoxUI
   <https://gitlab.com/thedrastical/iuvia/boxui>`. To add your work, you'll need
   to create a fork of the repository (which is just a copy of the repo under
   your personal account) and work over your fork until the contribution is
   ready. In order to push changes into your fork, you will need to change or
   add a remote with::

      $ git remote add origin https://github.com/user/repo.git

   You can see the information about the remotes with::

      $ git remote -v
      > origin  https://github.com/user/repo.git (fetch)
      > origin  https://github.com/user/repo.git (push)

2. **Make a new branch** in the repository where you will start to commit your
   changes. To make a branch in your repository you can do::

      $ git checkout -b bugfix/name-of-the-bugfix

3. **Commit your changes** when you are ready. Make atomic commits with
   comprehensible messages. If you follow these guidelines, you'll be alright:

   * Be concrete. And no, ``Fix bugs`` is not concrete.
   * Make atomic commits, one commit per task or fix.
   * Limit the subject line to 50 characters. If you need to add more details,
     you can add a longer description below, by separating the subject from the
     body with one blank line.
   * Use infinitive or imperative form of the verb. For example, ``Add tests to
     ...`` instead of ``Added tests to...``.
   * Capitalize the subject line, for example, ``Add tests`` instead of ``add
     tests``.
   * Do not end the subject line with a period.

4. **Push your changes** from your local branch to a remote branch of your own
   fork.

5. **Submit a Pull Request** (PR) to the development branch of the IUVIA
   repository.

6. And finally, **collaborate with the IUVIA team** while they review and
   accept your PR. You might be asked to change things, or requested
   explanations about your solution. The more you collaborate with this process,
   the easier it will be for us to accept your PR.


