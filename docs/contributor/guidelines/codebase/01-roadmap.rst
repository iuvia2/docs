Roadmap
=======

These are the items we are focusing on. We expect to give you an idea of what
our middle and long term future looks for us so that you can figure our where to
collaborate with us.

Each point is marked with labels that reflect each tasks' urgency within our
roadmap: :label:`badge-danger:urgent`, :label:`badge-warning:mid term` and
:label:`badge-success:long term`

* IUVIA Platform

  * ChubascOS:

    * iuvia.config :label:`badge-danger:urgent`
    * Package and distribute an installable build :label:`badge-warning:mid term`
    * Documentation :label:`badge-warning:mid term`
    * Internationalization :label:`badge-success:long term`

  * BoxUI:
      
    * Full UX/UI redesign :label:`badge-danger:urgent`
    * Device configuration features :label:`badge-danger:urgent`
    * Documentation :label:`badge-warning:mid term`
    * Internationalization :label:`badge-success:long term`

  * IUVIA Marketplace:

    * Continuous Integration tool to package and publish apps :label:`badge-warning:mid term`
    * Migration to Hyper :label:`badge-warning:mid term`
    * Documentation :label:`badge-warning:mid term`

  * IUVIA Onboarding and Setup Wizard:

    * Full UX/UI redesign :label:`badge-warning:mid term`
    * Testing and user validation :label:`badge-warning:mid term`
    * Documentation :label:`badge-warning:mid term`
    * Internationalization :label:`badge-success:long term`

* IUVIA Device

  * Firmware documentation :label:`badge-warning:mid term`
  * Energy efficiency management :label:`badge-success:long term`
  * Panic button :label:`badge-success:long term`
   
* IUVIA Distribution

  * Create online subscription platform :label:`badge-danger:urgent`
  * Integration services

    * Integration of DNS provider service :label:`badge-danger:urgent`
    * Integration of VPN provider service :label:`badge-danger:urgent`
    * Email relay :label:`badge-danger:urgent`

* Testing

  * Platform Apps User Acceptance :label:`badge-danger:urgent`
  * BoxUI and IUVIA administration process :label:`badge-warning:mid term`
  * Onboarding process :label:`badge-warning:mid term`
  * Integrated Hardware and Platform User Acceptance :label:`badge-success:long term`

* Reaching customers

  * Crowdfunding campaign :label:`badge-danger:urgent`

* Privacy advocacy 

  * Conferences and talks :label:`badge-danger:urgent`
  * Content creation for blog and podcast :label:`badge-danger:urgent`

* Expanding the community

  * Reaching projects to collaborate with :label:`badge-danger:urgent`
  * Packaging Standards

    * Create packaging tools :label:`badge-danger:urgent`
    * Packaging documentation :label:`badge-danger:urgent`
    * Internationalization of packaging documentation :label:`badge-success:long term`

  * Packaging AppImages

    * Packaging backup and file storage AppImages :label:`badge-warning:mid term`
    * Packaging email client AppImages :label:`badge-warning:mid term`
    * Packaging calendar and contact related AppImages :label:`badge-warning:mid term`
    * Packaging web self-hosting AppImage :label:`badge-warning:mid term`
    * Packaging blog self-hosting AppImage :label:`badge-warning:mid term`
    * Packaging instant messaging AppImages :label:`badge-success:long term` 
    * Packaging decentralized node AppImages :label:`badge-success:long term` 
