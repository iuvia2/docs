Organization of our repositories
================================

In this guide, we divide all the different repositories into three big sections:

The **IUVIA Device repositories** are those that hold all the information and
implementatio related to the development of the IUVIA hardware device, firmware,
cad files, benchmarks, etc.

The **IUVIA Platform repositories** are the software related repos, containing the
development of our OS ChubascOS, and all the different software pieces: the
admin management interface, the P2P marketplace, etc.

Finally, the **IUVIA Project reposotories** hold everything else that helps IUVIA
become a known platform: our website, this documentation, infrastructure for
testing, etc.


IUVIA Device repositories
-------------------------

.. repo:: IUVIA ioboard
   :tags: hardware kicad

   KiCAD Files for the hardware design of the PCB.

   :ref:`Read more <contributor_hardware_overview>`

   :gitlab:`https://gitlab.com/thedrastical/iuvia/ioboard`


.. repo:: IUVIA ioboard-firmware
   :tags: hardware firmware

   This software controls the LED user interface in the IUVIA hardware, and
   other functions of the IO board like power measurement.
   
   :ref:`Read more <contributor_hardware_overview>`

   :gitlab:`https://gitlab.com/thedrastical/iuvia/ioboard-firmware`


.. repo:: IUVIA cad
   :tags: hardware cad

   CAD/CAM files for IUVIA enclosure and related parts.

   :gitlab:`https://gitlab.com/thedrastical/iuvia/cad`


.. repo:: Hardware benchmarks
   :tags: hardware

   Benchmarks performed to ascertain the real difference in power between the
   two platforms considered for the IUVIA Device: APU2D4 and ODROID H2. The
   indicators tested were related to raw CPU power, crypto operations, I/O and
   networking, which are the primary usage drivers of the hardware platform for
   our expected use-cases.
   
   :ref:`Read more <contributor_hardware_overview>`

   :gitlab:`https://gitlab.com/thedrastical/iuvia/hardware-benchmarks`



IUVIA Platform repositories
---------------------------

.. repo:: IUVIA BoxUI
   :tags: software python django

   User interface webapp for the IUVIA Device. BoxUI allows you to control the
   device settings, manage users, install update or delete apps and other
   things.
   
   :ref:`Read more <admin_guide>`

   :gitlab:`https://gitlab.com/thedrastical/iuvia/boxui`
   :docpages:`https://thedrastical.gitlab.io/iuvia/docs/`


.. repo:: Dat AppStore
   :tags: software p2p dat marketplace

   Implementation of the IUVIA P2P marketplace using Dat, structured for
   its packaging as self-describing AppImage to interact with the IUVIA
   Platform.

   :gitlab:`https://gitlab.com/thedrastical/iuvia/dat-appstore`
   :docpages:`https://thedrastical.gitlab.io/iuvia/dat-appstore/`


.. repo:: IUVIA Subscription Services
   :tags: software python django

   Web application to handle IUVIA Subscription Services, implemented in Django

   :gitlab:`https://gitlab.com/thedrastical/iuvia/iss`


.. repo:: AppImage directory and generation tools 
   :tags: software appimages bash docker

   This repository contains the directory of AppImages packaged by IUVIA and the
   tools necesasy to package AppImages.
   
   :ref:`Read more <contributor_appimages>`

   :gitlab:`https://gitlab.com/thedrastical/iuvia/appimage-repository`


.. repo:: ChubascOS
   :tags: software os debian

   This repository contains a customized Debian installation that will be used
   as a base for the IUVIA Platform
   
   :ref:`Read more <contributor_software_abstractions>`
   
   :gitlab:`https://gitlab.com/thedrastical/iuvia/chubascos`



IUVIA Project repositories
--------------------------

.. repo:: iuvia.io
   :tags: website html javascript css

   The repo of the IUVIA website `iuvia.io <https://iuvia.io>`_.

   :gitlab:`https://gitlab.com/thedrastical/iuvia/website`


.. repo:: Nothing to Hide ghost theme
   :tags: website ghost

   The repo of the ghost theme that we are using for our blog `Nothing to Hide
   <https://blog.iuvia.io>`_, based on the open theme `Saga
   <https://github.com/Reedyn/Saga>`_.
   
   :gitlab:`https://gitlab.com/thedrastical/iuvia/website`


.. repo:: IUVIA docs
   :tags: docs sphinx

   This documentation, done with sphinx.

   :gitlab:`https://gitlab.com/thedrastical/iuvia/docs`
   :docpages:`https://thedrastical.gitlab.io/iuvia/docs/`


.. repo:: users.iuvia.io
   :tags: infrastructure docker yunohost

   Docker-compose for the configuration of the testing server users.iuvia.io,
   with the configuration of nginx and letsencrypt
   
   :gitlab:`https://gitlab.com/thedrastical/iuvia/users.iuvia.io`
