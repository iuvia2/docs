How can you help?
#################

Do you like the IUVIA Project? Do you believe in the cause and want to see it
grow and reach more users?

Great! You can help us do it! Here are some things that you can do with us


.. figure:: /_static/images/iuvita-activist.png
    :width: 250px
    :align: center
    :alt: Let's burst the cloud together!

    Let's burst the cloud together!


Spreading the message
=====================

Talk about self-hosting, data sovereignty and mention IUVIA as one of the
solutions. You can talk about it in your social media, to your family and
friends, in conferences and talks or at your work.

You can :ref:`contact us<contributor_get_in_touch>` and ask for help or
information. We are also pretty active on spreading the word, so we are always
happy to talk and write about data privacy, free software and more!


Testing and reporting bugs
==========================

We need people to test IUVIA and give us feedback. :ref:`Get in touch with
us<contributor_get_in_touch>` if you want to be part of the tester pool. If you
are already a IUVIA user and want to report a bug, you can add it to our
repositories issue boards. Before you do that, make sure that you learn about
our :ref:`different repos <contributor_repositories>` and that you read the
:ref:`guidelines for reporting bugs <contributor_git_guidelines>`.


Translating
===========

We want to reach as many places as possible, and we'd love your help to
translate the IUVIA Platform and all the packaged apps to different
languages.

We will soon be adding a collaborative translation tool so that you can help us
with the IUVIA Platform Internationalization.


App packaging
=============

Help us make the IUVIA App ecosystem wider by packaging new apps and helping
more users get awesome cloud-like services while keeping their data at home.
You can learn how to do this in the
:ref:`contributor_package_your_app` section of this documentation (Coming soon).

Do you want some ideas on what apps to package? Check out `this
document <https://somewhere>`_.

If you have an app that you think belongs to this ecosystem, you are welcome to
package and upload to our marketplace. If you have any doubt please :ref:`contact us <contributor_get_in_touch>`
and we'll be happy to help. We are always looking forward to making this
community bigger!


Developing
==========

We are always looking for new collaborators. You can get involved in the
development of IUVIA no matter your skillset. We welcome coders, hardware
builders, designers... you name it! Learn how can you
:ref:`contribute to the codebase<contributor_codebase>` or :ref:`get in
touch with us<contributor_get_in_touch>`


Documenting
===========

This documentation is an evergrowing project. You are welcome to contribute to
it with instructions, correcting typos or adding clarifications that you
consider are necessary.
