.. _contributor_app_whishlist:

App Whishlist
#############

The decentralized app ecosystem is wide and evergrowing. In IUVIA we want to be
able to offer you as many applications in our P2P Marketplace as possible. For
that, we welcome colaborations to package and upload 3rd party apps.

To know how to package and upload applications, check out the
:ref:`contributor_package_your_app` section of this documentation (Coming soon).

If you want any ideas of what free software applications you can package and
include in the marketplace, here's a list of suggestions. If there's anything
that you would like to suggest so that the community helps package, you can let
:ref:`contact us <contributor_get_in_touch>` and let us know.

* `Awesome self-hosted apps <https://github.com/awesome-selfhosted/awesome-selfhosted>`_
* `Ghost <https://ghost.org/>`_
* `Gitlab <https://gitlab.org/>`_
