Minimum requirements
====================

ChubascOS is an Operating System based on Debian 10 (Buster). The recommmended
system requirements for Debian 10 are:

* 2 GB RAM
* 2 GHz Dual Core Processor
* 10 GB Free Hard disk space
* Bootable Installation Media (USB/ DVD)
* Internet connectivity (Optional)


