.. _contributor_dev_environment:

Setting up the IUVIA Platform development environment
=====================================================

To setup the IUVIA development environment you will need to set up ChubascOS,
our Debian-based OS, on a virtual machine. In the ChubascOS repository you can
find a Vagrant and VirtualBox image for this. Here are the steps to setup the
development environment.


Requiremnts
-----------

Linux-based system with Vagrant and VirtualBox installed.

Install VirtualBox
^^^^^^^^^^^^^^^^^^

Follow the installation guide at https://www.virtualbox.org/wiki/Downloads


Install Vagrant
^^^^^^^^^^^^^^^

Follow the installation guide at https://www.vagrantup.com/docs/installation


Install ChubascOS
^^^^^^^^^^^^^^^^^

ChubascOS is a customized Debian 10 (Buster) installation that serves as the
base of the IUVIA Platform.

To install ChubascOS, clone the `ChubascOS repository
<https://gitlab.com/thedrastical/iuvia/chubascos>`_::

   $ git clone git@gitlab.com:thedrastical/iuvia/chubascos.git

To setup the first time, first do::

   $ bash pre-bootstrap.sh

This creates a vendor directory with BoxUI and AppImage repository. You can do
this manually by creating a `vendor` directory in the repository root filder and
cloning the `BoxUI <https://gitlab.com/thedrastical/iuvia/boxui>`_
and the `AppImage <https://gitlab.com/thedrastical/iuvia/appimage-repository>`_
repositories::

   $ mkdir vendor && cd vendor
   $ git clone git@gitlab.com:thedrastical/iuvia/boxui.git
   $ git clone git@gitlab.com:thedrastical/iuvia/appimage-repository.git

The final directory structure that you will have is::

   .
   ├── base
   │   ├── jinjaparser.py
   │   ├── nginx.gotmpl
   │   └── nginx.jinja
   ├── bootstrap.sh
   ├── Dockerfile
   ├── LICENSE
   ├── pre-bootstrap.sh
   ├── README.md
   ├── Vagrantfile
   └── vendor
       ├── appimage-repository
       └── boxui

Now, as normally, initiate and provision the virtual machine with::

   $ vagrant up

This normally sets up your working folder as /vagrant in the guest machine via
the vboxfs.  If you prefer to use other synching mechanisms (nfs, rsync, or
others), you can refer to the Vagrant documentation.

Development workflow
^^^^^^^^^^^^^^^^^^^^

When upgrading BoxUI to a different version (in vendor/boxui), you need to do
the following command to stop the old BoxUI instance::

   $ vagrant ssh sudo systemctl stop boxui.service

The new version will be started automatically from the boxui.socket on the
first connection.
