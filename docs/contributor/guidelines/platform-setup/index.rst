.. _contributor_guidelines_platform_setup:

Setting up the IUVIA Platform for yourself
##########################################

..
   .. toctree::
      :maxdepth: 1

      01-requirements
      02-standalone
      03-development-env
      04-subscription-mockup

.. include:: 01-requirements.rst
.. include:: 03-development-env.rst
