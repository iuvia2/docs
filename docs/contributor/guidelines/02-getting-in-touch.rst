.. _contributor_get_in_touch:

Get in touch
############

These are the ways you can contact us and places where you can find us:


Email
=====

* `info@iuvia.io <mailto:info@iuvia.io>`_: Our general email account
* `report@iuvia.io <mailto:info@iuvia.io>`_: Our community moderation email
  account
* `incoming+thedrastical-iuvia-issues-19408389-issue-@incoming.gitlab.com <mailto:incoming+thedrastical-iuvia-issues-19408389-issue-@incoming.gitlab.com>`_: Our GitLab Service Desk email account



Social media
============

* `Twitter @iuvia_ <https://twitter.com/iuvia_>`_
* `IUVIA Linkedin <https://www.linkedin.com/company/iuvia/>`_
* `Keybase <https://keybase.io/team/iuvia>`_


Chatrooms
=========

* **#iuvia** on irc.freenode.net
* **#iuvia:matrix.org** on Matrix
