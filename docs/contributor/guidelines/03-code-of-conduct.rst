Code of Conduct
###############

Introduction, what are the aspirations for IUVIA as a community

.. todo:: Write about the IUVIA aspirtions as a community

For short
=========

IUVIA is dedicated to providing a harassment-free experience for everyone,
regardless of gender, gender identity and expression, sexual orientation,
disability, physical appearance, body size, age, race, or religion. We do not
tolerate harassment of participants in any form.

This code of conduct applies to all IUVIA spaces, including real life or online
events, conversations occurring in our social media, chat channels, IRC, Matrix,
keybase, conversations occurring in the GitLab repositories or issue board, both
online and off. Anyone who violates this code of conduct may be sanctioned or
expelled from these spaces at the discretion of the IUVIA moderation team.

Some IUVIA spaces may have additional rules in place, which will be made clearly
available to participants. Participants are responsible for knowing and abiding
by these rules.


Anti-harassment policy
======================

IUVIA is dedicated to providing a harassment-free experience for everyone. We do
not tolerate harassment of participants in any form.

This code of conduct applies to all IUVIA spaces, including real life or online
events, conversations occurring in our social media, chat channels, IRC, Matrix,
keybase, conversations occurring in the GitLab repositories or issue board, both
online and off. Anyone who violates this code of conduct may be sanctioned or
expelled from these spaces at the discretion of the IUVIA moderation team.

Some IUVIA spaces may have additional rules in place, which will be made clearly
available to participants. Participants are responsible for knowing and abiding
by these rules.

Harassment includes:

* Offensive comments related to gender, gender identity and expression, sexual
  orientation, disability, mental illness, neuro(a)typicality, physical
  appearance, body size, age, race, or religion.
* Unwelcome comments regarding a person’s lifestyle choices and practices,
  including those related to food, health, parenting, drugs, and employment.
* Deliberate misgendering or use of ‘dead’ or rejected names.
* Gratuitous or off-topic sexual images or behaviour  in spaces where they’re
  not appropriate.
* Physical contact and simulated physical contact (eg, textual descriptions
  like “*hug*” or “*backrub*”) without consent or after a request to stop.
* Threats of violence.
* Incitement of violence towards any individual, including encouraging a person
  to commit suicide or to engage in self-harm.
* Deliberate intimidation.
* Stalking or following.
* Harassing photography or recording, including logging online activity for
  harassment purposes.
* Sustained disruption of discussion.
* Unwelcome sexual attention.
* Pattern of inappropriate social contact, such as requesting/assuming
  inappropriate levels of intimacy with others
* Continued one-on-one communication after requests to cease.
* Deliberate “outing” of any aspect of a person’s identity without their
  consent except as necessary to protect vulnerable people from intentional
  abuse.
* Publication of non-harassing private communication. 

IUVIA prioritizes marginalized people’s safety over privileged people’s comfort.
IUVIA moderation team reserves the right not to act on complaints regarding:

* ‘Reverse’ -isms, including ‘reverse racism,’ ‘reverse sexism,’ and ‘cisphobia’
* Reasonable communication of boundaries, such as “leave me alone,” “go away,”
  or “I’m not discussing this with you.”
* Communicating in a ‘tone’ you don’t find congenial
* Criticizing racist, sexist, cissexist, or otherwise oppressive behavior or
  assumptions 


Reporting
=========

If you are being harassed by a member of IUVIA or the community participating in
the IUVIA spaces, notice that someone else is being harassed, or have any other
concerns, please contact the IUVIA moderation team at `report@iuvia.io
<mailto:report@iuvia.io>`_. If the person who is harassing you is on the team,
they will recuse themselves from handling your incident. We will respond as
promptly as we can.

This code of conduct applies to IUVIA spaces, but if you are being harassed by a
member of IUVIA outside our spaces, we still want to know about it. We will take
all good-faith reports of harassment by IUVIA members, especially IUVIA founding
team, seriously. This includes harassment outside our spaces and harassment that
took place at any point in time. The abuse team reserves the right to exclude
people from IUVIA based on their past behavior, including behavior outside IUVIA
spaces and behavior towards people who are not in IUVIA.

In order to protect volunteers from abuse and burnout, we reserve the right to
reject any report we believe to have been made in bad faith. Reports intended to
silence legitimate criticism may be deleted without response.

We will respect confidentiality requests for the purpose of protecting victims
of abuse. At our discretion, we may publicly name a person about whom we’ve
received harassment complaints, or privately warn third parties about them, if
we believe that doing so will increase the safety of IUVIA members or the
general public. We will not name harassment victims without their affirmative
consent.


Consequences
============

Participants asked to stop any harassing behavior are expected to comply
immediately.

If a participant engages in harassing behavior, IUVIA moderation team may take
any action they deem appropriate, up to and including expulsion from all IUVIA
moderated spaces and identification of the participant as a harasser to other
IUVIA members or the general public.


License
=======

This text is released to the public domain under a `Creative Common Zero license
(CC0) <https://creativecommons.org/publicdomain/zero/1.0/>`_ and anyone is free
to copy, modify it, and use it.

Here is a list of other Code of Conducts and Anti-harassment policies released
under CC0:

* `Geek Feminism Anti-harassment Policy
  <https://geekfeminism.wikia.org/wiki/Community_anti-harassment/Policy>`_
* `IndieWeb Code of Conduct <https://indieweb.org/code-of-conduct>`_
