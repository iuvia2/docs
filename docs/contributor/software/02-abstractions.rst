.. _contributor_software_abstractions:

Description of the high-level abstractions
##########################################

The IUVIA Platform is made of several software components that are
used for different parts of the architecture. We identify the
following high-level abstractions and what we require from them in the
lines hereafter, and we later define them in terms of actual software
that we are using.


Application Lifecycle Management Service
========================================

The Application Lifecycle Management Service is a subsystem with
the task of installing, upgrading and removing applications, as
well as putting data in a way that can be consumed to be backed up
by other tools.

This interface configures system components, such as systemd, to
run and isolate user applications, giving them just enough
permissions to do what they need to, but restricting access to the
rest of the system.

Platform Management Interface
=============================

The Platform Management Interface includes functionality to:

#. Create and edit users in the Global User Database
#. Modify hardware and other low-level infrastructure, such as energy management
#. Handle backups
#. Handle network connectivity
#. Handle overlay networking (e.g., publishing services through Tor)

Global User Database
====================

The global user database contains the users that are known by your
platform. Applications can make use of of this data through the
standard :term:`LDAP` protocol, or through a
:term:`SSO`/:term:`OIDC` procedure.

Global Configuration Infrastructure
===================================

The configuration infrastructure holds configuration parameters for
each application. Applications expose the configuration parameters
that they require, which can be filled by the Platform Management
Interface when the applications are to be installed into the
system.

..
   Unified authentication and authorization framework through :term:`SSO` (future)
   ===============================================================================

