What is the IUVIA Platform from a developer standpoint
######################################################

* A set of APIs and conventions
* The implementation of such APIs and conventions

The IUVIA Platform is software that you can use to reduce your
dependency on third-party cloud services.

The Platform provides a foundation to build self-hosting applications
on top. Application data is isolated from one another, but they can
share certain data through specific means. For example, Nextcloud can
internally use the OnlyOffice Document Server to provide real-time
collaboration.
