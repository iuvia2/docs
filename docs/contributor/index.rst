.. _contributor_guide:

#######################
IUVIA Contributor Guide
#######################

As a collective, we have been working really hard on raising awareness about
data privacy through different communities and projects. These are still alive
and embody a strong base to reach more people to join our cause and community.
Our involvement within the community allows us to ask questions and gather
accomplices for our journey on fighting for our data sovereignty from the
physical ownership perspective.

You can learn more about the IUVIA Collective in `here <https://iuvia.io/about-us.html>`_.

.. rst-class:: heading 

   Join our mission!

Do you want to contribute to the project? Do you have any project that you thing
aligns with IUVIA? Do you want to package and offer your app through the IUVIA
platform? We would love to help you be part of the IUVIA project and the
ecosystem we are building.

In this section of the documentation you will learn how to join our missin, what
things you can help us with and how, where to find us and what guidelines to
follow in order to be a part of IUVIA.

You will also learn about the architecture behind IUVIA, all the technical
nuances of all its parts, both hardware and software.



.. toctree::
   :maxdepth: 2
   :caption: Contents

   guidelines/index
   hardware/index
   software/index
